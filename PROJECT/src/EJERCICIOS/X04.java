package EJERCICIOS;

import javax.swing.JOptionPane;

public class X04 {
	public static void main(String [] args){
		String radio=JOptionPane.showInputDialog("Introduce el radio");
		double numdouble=Double.parseDouble(radio);
		final double PI=3.1416;
		JOptionPane.showMessageDialog(null, PI*Math.pow(numdouble, 2));
	}
}
