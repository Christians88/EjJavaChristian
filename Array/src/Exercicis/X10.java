package Exercicis;
/*Crea un arrau de n�meros de un tama�o pasado por teclado, el array
 * contendr� n�meros aleatorios primos entre los n�meros deseados, por
 * �ltimo nos indica cual es el mayor de todos.
 * Haz un m�todo para comprobar que el n�mero aleatorio es primo,
 * puedes hacer todo los m�todos que necesites.*/

import javax.swing.JOptionPane;

public class X10 {
	public static void main(String[] args) {
		String texto=JOptionPane.showInputDialog("Introduce un tama�o");
	        int num[]=new int[Integer.parseInt(texto)];
	        rellenarNumPrimosAleatorioArray(num, 1, 3);
	        mostrarArray(num);
	    }
	 
	    public static void rellenarNumPrimosAleatorioArray(int lista[], int a, int b){
	 
	        int i=0;
	        while(i<lista.length){
	            int num=((int)Math.floor(Math.random()*(a-b)+b));
	            if (esPrimo(num)){
	                lista[i]=num;
	                i++;
	            }
	        }
	    }
	    private static boolean esPrimo (int num){       
	        if (num<=1){
	            return false;
	        }else{
	            int prueba;
	            int contador=0;
	            prueba=(int)Math.sqrt(num);
	            
	            for (;prueba>1;prueba--){
	                if (num%prueba==0){
	                    contador+=1;
	                }
	            }
	            return contador < 1;
	        }
	    }
	 
	    public static void mostrarArray(int lista[]){
	        for(int i=0;i<lista.length;i++){
	        	JOptionPane.showMessageDialog(null,"En el indice "+i+" esta el valor "+lista[i]);
	        }
	    }
	}
