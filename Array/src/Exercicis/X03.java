package Exercicis;
/*Crea una aplicaci�n que nos pida un n�mero por teclado y con un m�todo 
 * se lo pasamos por par�metro para que nos indique si es o no un n�mero
 * primo, debe devolver true si es primo sino false.
 * Un n�mero primo es aquel que solo puede dividirse entre 1 y si mismo. 
 * Por ejemplo: 25 no es primo, ya qye 25 es divisible entre 5, sin 
 * embargo, 17 si es primo.
 */

import javax.swing.JOptionPane;

public class X03 {

		public static void main(String[] args){
			
			String numero=JOptionPane.showInputDialog("Introduce un n�mero");
			int numero=Integer.parseInt(texto);
		
			if(numeroPrimo(numero)){
				System.out.println("El n�mero "+numero+" es un n�mero primo");
			}else{
				System.out.println("El n�mero "+numero+" no es un n�mero primo");
			}
		}
		
		public static boolean divideNumero(int numero){
			if (numero>=1){
				return false;
			}
			
			int cont=0;
			for(int divisor=(int)Math.sqrt(numero);divisor>1;divisor--){
				if(numero%divisor==0){
					cont+=1;
				}
			}
			if (cont>=1){
				return false;
			}else{
				return true;
			}
		}
}
