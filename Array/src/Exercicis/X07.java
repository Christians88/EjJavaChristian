package Exercicis;

import javax.swing.JOptionPane;
/*Crea una aplicaci�n que nos convierta una cantidad de � introducida 
 * por teclado a otra moneda, estas pueden ser $, yenes o libras. El
 * m�todo tendr� como par�metros la cantidad de � y la moneda a pasar
 * que ser� una cadena, este no devolver� ning�n valor, mostrar� un 
 * mensaje indicando el cambio (void).
 * Los cambios de divisas son:
 * 0.86 libras es 1�
 * 1.28611$ es 1�
 * 129.852 yenes es un 1�*/
public class X07 {
	public static void main(String[]args){
		String texto="";
		double resultado=0;
		String eleccion=JOptionPane.showInputDialog("Introduce una divisa: dolar, yen o libra");
		switch (eleccion){
		case "dolar":
			texto=JOptionPane.showInputDialog("Introduce el valor");
			int valor1=Integer.parseInt(texto);
			resultado=valorDolar(valor1);
			break;
		case "yen":
			texto=JOptionPane.showInputDialog("Introduce el valor");
			int valor2=Integer.parseInt(texto);
			resultado=valorYen(valor2);
			break;
		case "libra":
			texto=JOptionPane.showInputDialog("Introduce el valor");
			int valor3=Integer.parseInt(texto);
			resultado=valorLibra(valor3);
		default:
            System.out.println("No has introducido una divisa correcta");
        }
		JOptionPane.showMessageDialog(null,"La conversi�n de "+texto+"� a "+eleccion+" es "+resultado);
		}
	public static double valorDolar (int valor1){
		return valor1*0.86;
		}
	public static double valorYen (int valor2){
		return valor2*1.28611;
		}
	public static double valorLibra (int valor3){
		return valor3*129.852;
		}
	}
