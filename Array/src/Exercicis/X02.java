package Exercicis;
/*Crea una aplicaci�n que nos genere una cantidad de n�meros enteros aleatorios que nosotros
 * le pasaremos por teclado. Crea un m�todo donde pasamos como par�metros entre que n�meros
 * queremos que los genere, podemos pedirlas por teclado antes de generar los n�meros. Este
 * m�todo devolver� un n�mero entero aleatorio. Muestra estos n�meros por 
 * pantalla.
 * 
 */
import javax.swing.JOptionPane;

public class X02 {

		public static void main(String[]args){
			String texto=JOptionPane.showInputDialog("Introduce el l�mite de n�meros");
			int limite=Integer.parseInt(texto);
			texto=JOptionPane.showInputDialog("Introduce el n�mero m�s bajo");
			int numero1=Integer.parseInt(texto);
			texto=JOptionPane.showInputDialog("Introduce el n�mero m�s alto");
			int numero2=Integer.parseInt(texto);
			for(int i=0;i<limite;i++){
				System.out.println(generaNumero(numero1,numero2));
			}
			
			}
			public static int generaNumero(int numero1, int numero2){
				return ((int)Math.floor(Math.random()*(numero2-numero1)+numero1));
			
		}
}