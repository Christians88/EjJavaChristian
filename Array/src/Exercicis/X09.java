package Exercicis;

import javax.swing.JOptionPane;

/*Crea un array de n�meros donde le indicamos por teclado el tama�o del
 * array, rellenaremos el array con n�meros aleatorios entre 0 y 9, al
 * final muestra por pantalla el valor de cada posici�n y la suma de 
 * todos los valores. Haz un m�todo para rellenar el array (que tenga
 * como par�metros los n�meros entre los que tenga que generar), para
 * mostrar el contenido y la suma del array y un m�todo privado para
 * generar un n�mero aleatorio (lo puedes usar para otros ejercicios).*/

import javax.swing.JOptionPane;

public class X09 {
	public static void main(String[] args) {
		String texto=JOptionPane.showInputDialog("Introduce un tama�o");
		int num[]=new int[Integer.parseInt(texto)];
		rellenarNumAleatorioArray(num, 0, 9);
		mostrarArray(num);
	}

	public static void rellenarNumAleatorioArray(int lista[], int a, int b){
		for(int i=0;i<lista.length;i++){
			lista[i]=((int)Math.floor(Math.random()*(a-b)+b));
		}
	}

	public static void mostrarArray(int lista[]){
		for(int i=0;i<lista.length;i++){
			JOptionPane.showMessageDialog(null,"En el �ndice "+i+" est� el valor "+lista[i]);
		}
	}
}