/**
 * 
 */
package Exercicis;

import javax.swing.JOptionPane;

/*Crea una aplicaci�n que nos convierta una cantidad de � introducida 
 * por teclado a otra moneda, estas pueden ser $, yenes o libras. El
 * m�todo tendr� como par�metros la cantidad de � y la moneda a pasar
 * que ser� una cadena, este no devolver� ning�n valor, mostrar� un 
 * mensaje indicando el cambio (void).
 * Los cambios de divisas son:
 * 0.86 libras es 1�
 * 1.28611$ es 1�
 * 129.852 yenes es un 1�*/

public class X071 {
	public static double libras (double n1){
		double resultado=n1*0.86;
		return resultado;
	}
	public static double dolares (double n1){
		double resultado=n1*1.28611;
		return resultado;
	}
	public static double yenes (double n1){
		double resultado=n1*129.852;
		return resultado;
	}
	public static void main(String[] args) {
		String seleccion=JOptionPane.showInputDialog("�Qu� divisa quieres? \n"+"1 Libras \n"+"2 Dolares \n"+"3 Yenes " );
		switch(seleccion){
		case "1":
			String moneda=JOptionPane.showInputDialog("Introduce la cantidad para convertir en libras" );
			double cambio=Double.parseDouble(moneda);
			double resultado=libras(cambio);
			JOptionPane.showMessageDialog(null,"El cambio es  "+resultado+" libras");
			break;
		case "2":
			String moneda1=JOptionPane.showInputDialog("Introduce la cantidad para convertir en d�lares" );
			double cambio1=Double.parseDouble(moneda1);
			double resultado1=dolares(cambio1);
			JOptionPane.showMessageDialog(null,"El cambio es  "+resultado1+" dolares");
			break;
		case "3":
			String moneda2=JOptionPane.showInputDialog("Introduce la cantidad para convertir en yenes" );
			double cambio2=Double.parseDouble(moneda2);
			double resultado2=dolares(cambio2);
			JOptionPane.showMessageDialog(null,"El cambio es  "+resultado2+" yenes");
			break;
		}
	}
}